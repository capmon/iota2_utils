#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import os
import glob
import shutil
from itertools import chain, islice

import numpy as np
from tqdm import tqdm

import fiona
from shapely.geometry import shape, mapping
import rasterio
from rasterio import features
from rasterstats import zonal_stats
from jinja2 import Environment, FileSystemLoader
import zipfile


def chunks(iterable, size=100):
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))


def copy_shp(in_shp, out_shp):

    ext_shp = ["shp", "shx", "prj", "cpg", "dbf"]
    in_name_without_ext = in_shp[:-3]
    out_name_without_ext = out_shp[:-3]
    for ext in ext_shp:
        if not os.path.exists(in_name_without_ext+ext):
            continue
        shutil.copyfile(in_name_without_ext+ext, out_name_without_ext+ext)

    pass


def compute_mask(in_shapefile, base_raster, out_raster):
    """
    compute mask from shapefile using envelop of a base raster

    :param in_shapefile: shapefile to burn
    :param base_raster: raster to use a reference for out envelop/georef
    :param out_raster: out raster fullname
    :return: fullname
    """
    with rasterio.open(base_raster) as base:
        out_shape = base.shape
        out_transform = base.transform
        out_meta = base.meta.copy()
        out_crs = base.crs

    # print(" out_shape : {0}".format(out_shape))
    # print(" out_affine : {0}".format(out_affine))
    # print(" out_meta : {0}".format(out_meta))

    with fiona.open(in_shapefile, "r") as shapefile:
        feat_gen = ((feature["geometry"], 1) for feature in shapefile)
        image = features.rasterize(feat_gen, out_shape=out_shape, transform=out_transform)

    # print(image.shape)
    with rasterio.open(out_raster, "w", driver='GTiff', dtype=rasterio.uint8, count=1, width=out_shape[1],
                       height=out_shape[0], compress='lzw', transform=out_transform, crs=out_crs) as dest:
        # print(dest.width, dest.height)
        # print(dest.crs)
        # print(dest.transform)
        # print(dest.count)
        # print(dest.indexes)
        dest.write(image, 1)

    return out_raster


def mask_classif(in_classif, in_raster_mask, out_mask_classif=None, out_suffix=None):
    """
    masked classification with input raster mask

    :param in_classif:
    :param in_raster_mask:
    :param _out_mask_classif:
    :return:
    """
    with rasterio.open(in_classif, 'r') as in_class:
        with rasterio.open(in_raster_mask, 'r') as mask:
            array_classif = in_class.read(1)
            array_mask = mask.read(1)
            out_profile = in_class.profile.copy()

    masked_array = np.multiply(array_classif, array_mask)

    if out_mask_classif is None:
        out_dir = os.path.dirname(in_classif)
        out_basename = os.path.splitext(os.path.basename(in_classif))[0]
        if out_suffix is None:
            out_suffix = "masked"
        out_mask_classif = os.path.join(out_dir, out_basename+"_"+out_suffix+".tif")

    with rasterio.open(out_mask_classif, "w", **out_profile) as dest:
        dest.write(masked_array, 1)


def add_classif_parcel_stat_block(in_lpis, in_classif, block_size=1000, fields_suffix=None, border_size=None):
    """

    :param in_lpis:
    :param in_classif:
    :param block_size:
    :param fields_suffix:
    :param border_size:
    :return:
    """
    flush_size = 50000

    def format_cat_sat_dict(stat_dict, num_label=3):
        array_nb_line = len(stat_dict)
        if array_nb_line < num_label:
            array_nb_line = num_label

        stat_array = np.zeros((array_nb_line, 2))
        i = 0
        for item in stat_dict.items():
            stat_array[i][0] = item[0]
            stat_array[i][1] = item[1]
            i += 1

        ind = np.argsort(stat_array[:, -1])[::-1]
        stat_array_order = stat_array[ind]
        count = stat_array_order[:, -1].sum()
        if count > 0:
            stat_array_order[:, -1] = stat_array_order[:, -1]
        return stat_array_order[0:num_label]

    # create temporary result shapefile
    base_without_ext = os.path.splitext(os.path.basename(in_lpis))[0]
    in_lpis_dirname = os.path.dirname(in_lpis)
    out_tmp_shp = os.path.join(in_lpis_dirname, base_without_ext+"_tmp.shp")

    field_label1 = "L1_{0}".format(fields_suffix[0:7])
    conf_label1 = "C1_{0}".format(fields_suffix[0:7])
    field_label2 = "L2_{0}".format(fields_suffix[0:7])
    conf_label2 = "C2_{0}".format(fields_suffix[0:7])
    field_label3 = "L3_{0}".format(fields_suffix[0:7])
    conf_label3 = "C3_{0}".format(fields_suffix[0:7])

    with fiona.open(in_lpis) as src:

        meta = src.meta
        schema2 = src.schema.copy()
        schema2['properties'][field_label1] = "int:4"
        schema2['properties'][conf_label1] = "float"
        schema2['properties'][field_label2] = "int:4"
        schema2['properties'][conf_label2] = "float"
        schema2['properties'][field_label3] = "int:4"
        schema2['properties'][conf_label3] = "float"

        meta.update(schema=schema2)

        count_feat = len(src)
        print("nb_feat = {0}".format(count_feat))

        fetch_nb = block_size
        print("loop by block")
        dst = fiona.open(out_tmp_shp, 'w', **meta)

        process_nb = 0
        write_nb = 0
        for block_nb, chunk in enumerate(chunks(src, fetch_nb)):

            print("load chunk")
            curr_feat = [feat for feat in chunk]
            print("chunk len {0}".format(len(curr_feat)))
            if len(curr_feat) == 0:
                break

            stat_feat = []
            if border_size is not None:
                stat_feat = [feat.copy() for feat in curr_feat]
                print("buffer chunk")
                for feat in tqdm(stat_feat):
                    geom_buff = shape(feat['geometry']).buffer(-border_size)
                    if not geom_buff:
                        feat["geometry"] = None
                        continue
                    if not geom_buff.is_valid:
                        feat["geometry"] = None
                        continue

                    if geom_buff.is_empty:
                        feat["geometry"] = mapping(shape(feat['geometry']).representative_point())
                        continue
                    feat["geometry"] = mapping(geom_buff)
            else:
                stat_feat = curr_feat

            print(f"begin zonal_stat")
            out_stat = zonal_stats(stat_feat, in_classif, categorical=True)
            logging.info("first stat = {0}".format(out_stat[0]))
            stat_0 = format_cat_sat_dict(out_stat[0])
            logging.info("first stat format = {0}".format(stat_0))
            out_stat_iter = iter(out_stat)

            for f in tqdm(curr_feat):
                stat = next(out_stat_iter)
                format_stat = format_cat_sat_dict(stat, 3)
                f['properties'][field_label1] = int(format_stat[0][0])
                f['properties'][conf_label1] = format_stat[0][1]
                f['properties'][field_label2] = int(format_stat[1][0])
                f['properties'][conf_label2] = format_stat[1][1]
                f['properties'][field_label3] = int(format_stat[2][0])
                f['properties'][conf_label3] = format_stat[2][1]
                dst.write(f)
            del out_stat
            del stat_0

            process_nb += len(curr_feat)
            write_nb += len(curr_feat)
            # flush write data
            if write_nb >= flush_size:
                dst.close()
                dst = fiona.open(out_tmp_shp, 'a', **meta)
                write_nb = 0
            print(f"processed feature : {process_nb}")

        dst.close()

    # remove old shapefile
    in_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext)))
    for file in in_shape_files:
        os.remove(file)

    # rename temp to input
    tmp_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext+"_tmp")))
    for file in tmp_shape_files:
        file_ext = os.path.splitext(os.path.basename(file))[1]
        os.rename(file, os.path.join(in_lpis_dirname, base_without_ext+file_ext))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="post process classifications 2019")

    parser.add_argument("-i", "--in_classif_dir", dest="classif_dir",
                        help="root directory containing iota2 classification result", required=True)
    parser.add_argument("-rp", "--rpg_prep_dir", dest="rpg_prep_dir",
                        help="directory containing rpg preprocessing shapefile by tile", required=True)
    parser.add_argument("-o", "--out_root_dir", dest="out_root_dir", help="out result directory",
                        required=True)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # first get metadata from input classif dirname
    classif_dir = os.path.abspath(args.classif_dir)
    logging.info(f" input classif dir {classif_dir}")  # 30UXV_2019_NOV-AUG_RPGFOI_N73_S2-S1
    basename = os.path.basename(classif_dir)
    classif_meta = basename.split("_")
    tile = classif_meta[0]
    year = classif_meta[1]
    period = classif_meta[2]
    lpis_limit = classif_meta[3]
    nomenclature = classif_meta[4]
    classif_sensor = classif_meta[5]
    logging.info(f"s2 tile : {tile}")
    logging.info(f"year : {year}")
    logging.info(f"classif period : {period}")
    logging.info(f"classif lpis_limit : {lpis_limit}")
    logging.info(f"nomencalture : {nomenclature}")
    logging.info(f"classif_sensor : {classif_sensor}")

    # 2 create out_dir from out_root_dir and metadata
    # out_dir = out_root_dir/tile_year/{lpis_limit}_{nomenclature}_{classif_sensor}_{period}
    logging.info("create out dir")
    subdir_name = f"{lpis_limit}_{nomenclature}_{classif_sensor}_{period}"
    out_dir = os.path.join(args.out_root_dir, f"{tile}_{year}", subdir_name)
    os.makedirs(out_dir, exist_ok=True)

    # 3 copy classif result/rename in out_dir
    logging.info("copy data from classif result")

    out_prefix_classif = f"rpg_{year}_{tile}_{lpis_limit}_{nomenclature}_{classif_sensor}_{period}"

    classif_final_dir = os.path.join(classif_dir, "tmp", "final")
    classif_base = os.path.join(classif_final_dir, "Classifications_fusion.tif")
    classif_color = os.path.join(classif_final_dir, "Classifications_fusion_ColorIndexed.tif")
    out_classif = os.path.join(out_dir, f"{out_prefix_classif}_classif.tif")
    out_classif_color = os.path.join(out_dir, f"{out_prefix_classif}_classifColored.tif")
    shutil.copyfile(classif_base, out_classif)
    shutil.copyfile(classif_color, out_classif_color)

    # 4 copy prep rpg in out_dir
    logging.info("copy data from LPIS prepa")
    out_prefix_prep = f"rpg_{year}_{tile}_{lpis_limit}_{nomenclature}"
    rpg_dir = os.path.join(args.rpg_prep_dir, year, tile)
    logging.info(f"rpg_dir {rpg_dir}")
    lpis_type_dict = {
        "RPGINI": "rpg_ini",
        "RPGFOI": "rpg_foi",
        "CROPFOI": "crop_foi",
        "CROPINI": "lpis_crop"
    }
    in_rpg_prefix = f"prep_rpg_{year}_{tile}_{lpis_type_dict[lpis_limit]}"   # prep_rpg_2019_30UXV_rpg_ini_val.shp
    in_rpg_shp_val = os.path.join(rpg_dir, in_rpg_prefix+"_val.shp")
    in_rpg_shp_all = os.path.join(rpg_dir, in_rpg_prefix+"_all.shp")
    out_rpg_shp_val = os.path.join(out_dir, out_prefix_prep+"_val.shp")
    out_rpg_shp_all = os.path.join(out_dir, out_prefix_prep+"_all.shp")
    copy_shp(in_rpg_shp_val, out_rpg_shp_val)
    copy_shp(in_rpg_shp_all, out_rpg_shp_all)
    lpis_type_dict_b = {
        "RPGINI": "rpg_ini",
        "RPGFOI": "rpg_foi",
        "CROPFOI": "lpis_crop_foi",
        "CROPINI": "lpis_crop"
    }
    in_npix10b5_mask = os.path.join(rpg_dir, f"lpis_{tile}_npix10b5_{lpis_type_dict_b[lpis_limit]}.tif")
    in_npix20b10_mask = os.path.join(rpg_dir,f"lpis_{tile}_npix20b10_{lpis_type_dict_b[lpis_limit]}.tif")
    out_npix10b5_mask = os.path.join(out_dir, out_prefix_prep+"_npix10b5_mask.shp")
    out_npix20b10_mask = os.path.join(out_dir, out_prefix_prep+"_npix20b10_mask.shp")
    shutil.copyfile(in_npix10b5_mask, out_npix10b5_mask)
    shutil.copyfile(in_npix20b10_mask, out_npix20b10_mask)
    # lpis_30UXV_npix10b5_rpg_ini

    # 6 compute rpg mask
    logging.info("compute rpg mask")
    out_rpg_mask = os.path.join(out_dir, out_prefix_prep+"_mask"+".tif")
    compute_mask(out_rpg_shp_all, out_classif, out_rpg_mask)

    # 7 mask classif result
    logging.info("mask classif result")
    out_classif_mask = os.path.join(out_dir, f"{out_prefix_classif}_classif_masked.tif")
    out_classif_color_mask = os.path.join(out_dir, f"{out_prefix_classif}_classifColored_masked.tif")
    mask_classif(out_classif, out_rpg_mask, out_classif_mask)
    mask_classif(out_classif_color, out_rpg_mask, out_classif_color_mask)

    # 8 calc stat
    logging.info("add classif field to result shp")
    rpg_shp_result = os.path.join(out_dir, f"{out_prefix_classif}_result.shp")
    copy_shp(in_rpg_shp_all, rpg_shp_result)

    add_classif_parcel_stat_block(rpg_shp_result, out_classif, block_size=10000, fields_suffix=nomenclature)

    # 9 create qgis project from template
    logging.info("create qgis template")

    template_dir = os.path.join(args.out_root_dir, "qgis_template")
    env = Environment(loader=FileSystemLoader(template_dir))
    template_name = f"{tile}_{year}_qgis_template.qgs"
    template = env.get_template(template_name)
    # template_var = {
    #     "maj_v2_qa": test_stat_df
    # }
    template_var = {
        "subdir": subdir_name,
        "classif_dataset": {
            "id": f"rpg_{year}_{tile}_{lpis_limit}_train_val",
            "name": f"{lpis_limit}_train_val",
            "source": f"{out_prefix_prep}_all.shp"
        },
        "classif_result": {
            "id": f"rpg_{year}_{tile}_{lpis_limit}_result",
            "name": f"{lpis_limit}_result",
            "source": f"{out_prefix_classif}_result.shp"
        },
        "declaration": {
            "id": f"rpg_{year}_{tile}_{lpis_limit}_decla",
            "name": f"{lpis_limit}_declaration",
            "source": f"rpg_{year}_{tile}_{lpis_limit}_{nomenclature}_all.shp"
        },
        "classif": {
            "id": f"{lpis_limit}_classif_{nomenclature}_{classif_sensor}_{period}_masked",
            "name": f"{lpis_limit}_classif_{nomenclature}_{classif_sensor}_{period}",
            "source": f"{out_prefix_classif}_classif_masked.tif"
        }
    }
    output_from_parsed_template = template.render(**template_var)
    qgs_name = f'{tile}_{year}_{lpis_limit}_{nomenclature}_{classif_sensor}_{period}'
    out_dir_qgz = os.path.join(args.out_root_dir, f"{tile}_{year}")

    out_qgs = os.path.join(out_dir_qgz, qgs_name+".qgs")
    with open(out_qgs, "wb") as fh:
        fh.write(output_from_parsed_template.encode('UTF-8'))
    out_qgd = os.path.join(out_dir_qgz, qgs_name+".qgd")
    shutil.copyfile(os.path.join(args.out_root_dir, "qgis_template", "visu_qgis_template.qgd"), out_qgd)

    out_qgz = os.path.join(out_dir_qgz, qgs_name+".qgz")
    zipf = zipfile.ZipFile(out_qgz, 'w', zipfile.ZIP_DEFLATED)
    zipf.write(out_qgs, os.path.basename(out_qgs))
    zipf.write(out_qgd, os.path.basename(out_qgd))
    zipf.close()

    os.remove(out_qgd)
    os.remove(out_qgs)

