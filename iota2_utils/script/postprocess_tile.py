#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging

import os
import sys
import glob
import shutil
import rasterio
from rasterio import features
import fiona
from shapely.geometry import shape, mapping
from tqdm import tqdm
from itertools import chain, islice
import numpy as np
from rasterstats import zonal_stats


def compute_mask(in_shapefile, base_raster, out_raster):
    """
    compute mask from shapefile using envelop of a base raster

    :param in_shapefile: shapefile to burn
    :param base_raster: raster to use a reference for out envelop/georef
    :param out_raster: out raster fullname
    :return: fullname
    """
    with rasterio.open(base_raster) as base:
        out_shape = base.shape
        out_transform = base.transform
        out_meta = base.meta.copy()
        out_crs = base.crs

    # print(" out_shape : {0}".format(out_shape))
    # print(" out_affine : {0}".format(out_affine))
    # print(" out_meta : {0}".format(out_meta))

    with fiona.open(in_shapefile, "r") as shapefile:
        feat_gen = ((feature["geometry"], 1) for feature in shapefile)
        image = features.rasterize(feat_gen, out_shape=out_shape, transform=out_transform)

    # print(image.shape)
    with rasterio.open(out_raster, "w", driver='GTiff', dtype=rasterio.uint8, count=1, width=out_shape[1],
                       height=out_shape[0], compress='lzw', transform=out_transform, crs=out_crs) as dest:
        # print(dest.width, dest.height)
        # print(dest.crs)
        # print(dest.transform)
        # print(dest.count)
        # print(dest.indexes)
        dest.write(image, 1)

    return out_raster


def copy_classif_final(in_classif_dir, out_dir, out_suffix=None):
    """
    copy iota2 classif result in outpur dir and rename

    :param in_classif_dir:
    :param out_dir:
    :param out_name:
    :param out_prefix:
    :return:
    """
    classif_base = os.path.join(in_classif_dir, "tmp", "final", "Classif_Seed_0.tif")
    classif_color = os.path.join(in_classif_dir, "tmp", "final", "Classif_Seed_0_ColorIndexed.tif")
    classif_conf = os.path.join(in_classif_dir, "tmp", "final", "Confidence_Seed_0.tif")

    if not os.path.exists(classif_base):
        logging.error("base classif  {0} doesn't exists".format(classif_base))
        sys.exit(0)

    suffix_dict = {
        "LBA_05": "N17_Avril",
        "LBA_07": "N17_Juillet",
        "LBA_11": "N17_Novembre",
        "LBB_05": "N43_Avril",
        "LBB_07": "N43_Juillet",
        "LBB_11": "N43_Novembre",
        "N17_MAY": "N17_Avril",
        "N17_AUG": "N17_Juillet",
        "N17_NOV": "N17_Novembre",
        "N43_MAY": "N43_Avril",
        "N43_AUG": "N43_Juillet",
        "N43_NOV": "N43_Novembre"
    }

    if out_suffix is None:
        in_dirname = os.path.basename(in_classif_dir)
        in_suffix = "_".join(in_dirname.split("_")[4:])
        logging.info(" copy classif in_suffix = {0}".format(in_suffix))
        if in_suffix in suffix_dict:
            out_suffix = suffix_dict[in_suffix]
        else:
            out_suffix = in_suffix

    logging.info(" copy classif out_suffix = {0}".format(out_suffix))

    out_classif = os.path.join(out_dir, "Classif_"+out_suffix+".tif")
    out_classif_color = os.path.join(out_dir, "Classif_ColorIndex_"+out_suffix+".tif")
    out_conf = os.path.join(out_dir, "Confidence_"+out_suffix+".tif")

    shutil.copyfile(classif_base, out_classif)
    logging.info(" copy {0} to {1}".format(classif_base, out_classif))
    shutil.copyfile(classif_color, out_classif_color)
    logging.info(" copy {0} to {1}".format(classif_color, out_classif_color))
    shutil.copyfile(classif_conf, out_conf)
    logging.info(" copy {0} to {1}".format(classif_conf, out_conf))

    return out_classif, out_classif_color, out_conf


def mask_classif(in_classif, in_raster_mask, out_mask_classif=None, out_suffix=None):
    """
    masked classification with input raster mask

    :param in_classif:
    :param in_raster_mask:
    :param _out_mask_classif:
    :return:
    """
    with rasterio.open(in_classif, 'r') as in_class:
        with rasterio.open(in_raster_mask, 'r') as mask:
            array_classif = in_class.read(1)
            array_mask = mask.read(1)
            out_profile = in_class.profile.copy()

    masked_array = np.multiply(array_classif, array_mask)

    if out_mask_classif is None:
        out_dir = os.path.dirname(in_classif)
        out_basename = os.path.splitext(os.path.basename(in_classif))[0]
        if out_suffix is None:
            out_suffix = "masked"
        out_mask_classif = os.path.join(out_dir, out_basename+"_"+out_suffix+".tif")

    with rasterio.open(out_mask_classif, "w", **out_profile) as dest:
        dest.write(masked_array, 1)


def add_classif_parcel_stat(in_lpis, in_classif, border_size=0, fields_suffix=None):
    """

    :param in_lpis:
    :param in_classif:
    :param border_size:
    :return:
    """

    out_stat = zonal_stats(in_lpis, in_classif, categorical=True)
    logging.info("first stat = {0}".format(out_stat[0]))

    def format_cat_sat_dict(stat_dict, num_label=3):
        array_nb_line = len(stat_dict)
        if array_nb_line < num_label:
            array_nb_line = num_label

        stat_array = np.zeros((array_nb_line, 2))
        i = 0
        for item in stat_dict.items():
            stat_array[i][0] = item[0]
            stat_array[i][1] = item[1]
            i += 1

        ind = np.argsort(stat_array[:, -1])[::-1]
        stat_array_order = stat_array[ind]
        count = stat_array_order[:, -1].sum()
        if count > 0:
            stat_array_order[:, -1] = stat_array_order[:, -1]
        return stat_array_order[0:num_label]

    stat_0 = format_cat_sat_dict(out_stat[0])
    logging.info("first stat format = {0}".format(stat_0))

    out_stat_iter = iter(out_stat)
    # create temporary result shapefile
    base_without_ext = os.path.splitext(os.path.basename(in_lpis))[0]
    in_lpis_dirname = os.path.dirname(in_lpis)
    out_tmp_shp = os.path.join(in_lpis_dirname, base_without_ext+"_tmp.shp")

    field_label1 = "L1_{0}".format(fields_suffix[0:7])
    conf_label1 = "C1_{0}".format(fields_suffix[0:7])
    field_label2 = "L2_{0}".format(fields_suffix[0:7])
    conf_label2 = "C2_{0}".format(fields_suffix[0:7])
    field_label3 = "L3_{0}".format(fields_suffix[0:7])
    conf_label3 = "C3_{0}".format(fields_suffix[0:7])

    with fiona.open(in_lpis) as src:

        meta = src.meta
        schema2 = src.schema.copy()
        schema2['properties'][field_label1] = "int:4"
        schema2['properties'][conf_label1] = "float"
        schema2['properties'][field_label2] = "int:4"
        schema2['properties'][conf_label2] = "float"
        schema2['properties'][field_label3] = "int:4"
        schema2['properties'][conf_label3] = "float"

        meta.update(schema=schema2)

        with fiona.open(out_tmp_shp, 'w', **meta) as dst:
            for f in src:
                stat = next(out_stat_iter)
                format_stat = format_cat_sat_dict(stat, 3)
                f['properties'][field_label1] = int(format_stat[0][0])
                f['properties'][conf_label1] = format_stat[0][1]
                f['properties'][field_label2] = int(format_stat[1][0])
                f['properties'][conf_label2] = format_stat[1][1]
                f['properties'][field_label3] = int(format_stat[2][0])
                f['properties'][conf_label3] = format_stat[2][1]
                dst.write(f)

    # remove old shapefile
    in_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext)))
    for file in in_shape_files:
        os.remove(file)

    # rename temp to input
    tmp_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext+"_tmp")))
    for file in tmp_shape_files:
        file_ext = os.path.splitext(os.path.basename(file))[1]
        os.rename(file, os.path.join(in_lpis_dirname, base_without_ext+file_ext))


def chunks(iterable, size=100):
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))


def add_classif_parcel_stat_block(in_lpis, in_classif, block_size=1000, fields_suffix=None, border_size=None):
    """

    :param in_lpis:
    :param in_classif:
    :param border_size:
    :return:
    """
    flush_size = 50000

    def format_cat_sat_dict(stat_dict, num_label=3):
        array_nb_line = len(stat_dict)
        if array_nb_line < num_label:
            array_nb_line = num_label

        stat_array = np.zeros((array_nb_line, 2))
        i = 0
        for item in stat_dict.items():
            stat_array[i][0] = item[0]
            stat_array[i][1] = item[1]
            i += 1

        ind = np.argsort(stat_array[:, -1])[::-1]
        stat_array_order = stat_array[ind]
        count = stat_array_order[:, -1].sum()
        if count > 0:
            stat_array_order[:, -1] = stat_array_order[:, -1]
        return stat_array_order[0:num_label]

    # create temporary result shapefile
    base_without_ext = os.path.splitext(os.path.basename(in_lpis))[0]
    in_lpis_dirname = os.path.dirname(in_lpis)
    out_tmp_shp = os.path.join(in_lpis_dirname, base_without_ext+"_tmp.shp")

    field_label1 = "L1_{0}".format(fields_suffix[0:7])
    conf_label1 = "C1_{0}".format(fields_suffix[0:7])
    field_label2 = "L2_{0}".format(fields_suffix[0:7])
    conf_label2 = "C2_{0}".format(fields_suffix[0:7])
    field_label3 = "L3_{0}".format(fields_suffix[0:7])
    conf_label3 = "C3_{0}".format(fields_suffix[0:7])

    with fiona.open(in_lpis) as src:

        meta = src.meta
        schema2 = src.schema.copy()
        schema2['properties'][field_label1] = "int:4"
        schema2['properties'][conf_label1] = "float"
        schema2['properties'][field_label2] = "int:4"
        schema2['properties'][conf_label2] = "float"
        schema2['properties'][field_label3] = "int:4"
        schema2['properties'][conf_label3] = "float"

        meta.update(schema=schema2)

        count_feat = len(src)
        print("nb_feat = {0}".format(count_feat))

        fetch_nb = block_size
        print("loop by block")
        dst = fiona.open(out_tmp_shp, 'w', **meta)

        process_nb = 0
        write_nb = 0
        for block_nb, chunk in enumerate(chunks(src, fetch_nb)):

            print("load chunk")
            curr_feat = [feat for feat in chunk]
            print("chunk len {0}".format(len(curr_feat)))
            if len(curr_feat) == 0:
                break

            stat_feat = []
            if border_size is not None:
                stat_feat = [feat.copy() for feat in curr_feat]
                print("buffer chunk")
                for feat in tqdm(stat_feat):
                    geom_buff = shape(feat['geometry']).buffer(-border_size)
                    if not geom_buff:
                        feat["geometry"] = None
                        continue
                    if not geom_buff.is_valid:
                        feat["geometry"] = None
                        continue

                    if geom_buff.is_empty:
                        feat["geometry"] = mapping(shape(feat['geometry']).representative_point())
                        continue
                    feat["geometry"] = mapping(geom_buff)
            else:
                stat_feat = curr_feat

            print(f"begin zonal_stat")
            out_stat = zonal_stats(stat_feat, in_classif, categorical=True)
            logging.info("first stat = {0}".format(out_stat[0]))
            stat_0 = format_cat_sat_dict(out_stat[0])
            logging.info("first stat format = {0}".format(stat_0))
            out_stat_iter = iter(out_stat)

            for f in tqdm(curr_feat):
                stat = next(out_stat_iter)
                format_stat = format_cat_sat_dict(stat, 3)
                f['properties'][field_label1] = int(format_stat[0][0])
                f['properties'][conf_label1] = format_stat[0][1]
                f['properties'][field_label2] = int(format_stat[1][0])
                f['properties'][conf_label2] = format_stat[1][1]
                f['properties'][field_label3] = int(format_stat[2][0])
                f['properties'][conf_label3] = format_stat[2][1]
                dst.write(f)
            del out_stat
            del stat_0

            process_nb += len(curr_feat)
            write_nb += len(curr_feat)
            # flush write data
            if write_nb >= flush_size:
                dst.close()
                dst = fiona.open(out_tmp_shp, 'a', **meta)
                write_nb = 0
            print(f"processed feature : {process_nb}")

        dst.close()

    # remove old shapefile
    in_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext)))
    for file in in_shape_files:
        os.remove(file)

    # rename temp to input
    tmp_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext+"_tmp")))
    for file in tmp_shape_files:
        file_ext = os.path.splitext(os.path.basename(file))[1]
        os.rename(file, os.path.join(in_lpis_dirname, base_without_ext+file_ext))


def add_join_field(in_shp, join_shp, join_field):
    """
    add join_field by inside point spatial join

    add a field from a shapefile to another by joining input shape and join shape
    when an inside point of input feature are inside join feature.

    :param in_shp:
    :param join_shp:
    :param join_field:
    :return:
    """
    # create temporary result shapefile
    base_without_ext = os.path.splitext(os.path.basename(in_shp))[0]
    in_lpis_dirname = os.path.dirname(in_shp)
    out_tmp_shp = os.path.join(in_lpis_dirname, base_without_ext+"_tmp.shp")
    with fiona.open(in_shp) as src:
        meta = src.meta
        schema2 = src.schema.copy()

        with fiona.open(join_shp) as join:
            schema2['properties'][join_field]=join.schema['properties'][join_field]
            meta.update(schema=schema2)

            with fiona.open(out_tmp_shp, 'w', **meta) as dst:
                for f in src:
                    f_shape = shape(f['geometry'])
                    bbox = f_shape.bounds
                    inside_point = f_shape.representative_point()
                    hits = list(join.items(bbox=bbox))
                    for join_feat in hits:
                        # print(join_feat[1])
                        if inside_point.within(shape(join_feat[1]['geometry'])):
                            f['properties'][join_field] = join_feat[1]['properties'][join_field]
                            break
                    dst.write(f)

    # remove old shapefile
    in_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext)))
    for file in in_shape_files:
        os.remove(file)

    # rename temp to input
    tmp_shape_files = glob.glob(os.path.join(in_lpis_dirname, '*{0}.*'.format(base_without_ext+"_tmp")))
    for file in tmp_shape_files:
        file_ext = os.path.splitext(os.path.basename(file))[1]
        os.rename(file, os.path.join(in_lpis_dirname, base_without_ext+file_ext))


def export_rpg_shapefile(in_shp, out_shp, remove_fields=None, rename_fields=None):
    """
    export rpg shapefile preprocess for classification to result rpg

    remove some fields only used for classification and test and rename label field with more explicit name

    :param in_shp:
    :param out_shp:
    :param remove_fields:
    :param rename_fields:
    :return:
    """
    with fiona.open(in_rpg_shp) as src:

        meta = src.meta
        schema2 = src.schema.copy()
        for field in remove_fields:
            if field in schema2['properties']:
                schema2['properties'].pop(field)
        for field in rename_fields:
            if field in schema2['properties']:
                value = schema2['properties'].pop(field)
                schema2['properties'][rename_fields[field]] = value
        if "SURF_ADM" in schema2['properties']:
            schema2['properties']["SURF_ADM"] = "float:18"
        if "COMPACITY" in schema2['properties']:
            schema2['properties']["COMPACITY"] = "float:18"
        meta.update(schema=schema2)

        with fiona.open(result_rpg_shp, 'w', **meta) as dst:
            for f in src:
                for field in remove_fields:
                    if field in f['properties']:
                        f['properties'].pop(field)
                for field in rename_fields:
                    if field in f['properties']:
                        value = f['properties'].pop(field)
                        f['properties'][rename_fields[field]] = value
                # if "SURF_ADM" in f['properties']:
                #    f['properties']["SURF_ADM"] = round(f['properties']["SURF_ADM"], 3)
                dst.write(f)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="post process classifications")

    parser.add_argument("-i", "--in_classif_dir", dest="classif_dir",
                        help="root directory containing iota2 classification result", required=True)
    parser.add_argument("-t", "--id_tile", dest="id_tile", help="id of the S2 tile to postprocess",
                        required=True)
    parser.add_argument("-rp", "--rpg_prep_dir", dest="rpg_prep_dir",
                        help="directory containing rpg preprocessing shapefile by tile", required=True)
    parser.add_argument("-ra", "--rpg_asp_dir", dest="rpg_asp_dir",
                        help="directory containing rpg from asp exported by tile", required=True)
    parser.add_argument("-y", "--year", dest="year", help="year of classification and rpg data", required=True)
    parser.add_argument("-o", "--out_dir", dest="out_dir", help="out result directory",
                        required=True)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # first get S tile id
    id_tile = args.id_tile.upper()
    logging.info("tile to postprocess : {id_tile}".format(id_tile=id_tile))

    # then get all classification result containing this id
    classif_root_dir = args.classif_dir
    if not os.path.exists(classif_root_dir):
        logging.error("root classif dir {0} doesn't exists".format(classif_root_dir))
        sys.exit(0)

    tile_classifs_list = glob.glob(os.path.join(classif_root_dir, '*{0}*'.format(id_tile)))
    logging.info(" {0} classifications found for tile {id_tile} :".format(len(tile_classifs_list), id_tile=id_tile))
    for classif in tile_classifs_list:
        logging.info("\t {0}".format(classif))

    # find the directory containing rpg for this tile
    rpg_root_dir = args.rpg_prep_dir
    rpg_tile_dirs = glob.glob(os.path.join(rpg_root_dir, '*{0}*'.format(id_tile)))
    if len(rpg_tile_dirs) == 0:
        logging.error("rpg_dir containing rpg shapefile for tile {0} don't found in {1}".format(id_tile, rpg_root_dir))
        sys.exit(0)
    if len(rpg_tile_dirs) > 1:
        logging.warning("more than one directory found for rpg tile {0}, use the first".format(id_tile))

    rpg_tile_dir = rpg_tile_dirs[0]
    logging.info(" rpg tile dir  {0}".format(rpg_tile_dir))

    #################################################################################################################
    # begin post processing
    # 0 if out_dir not existing create it
    out_dir = args.out_dir
    if not (os.path.exists(out_dir)):
        os.makedirs(out_dir)

    # 1 copy preprocess rpg and tile region
    # rpg init file is the file with shorter name and ending by tile id
    rpg_shape_files = glob.glob(os.path.join(rpg_tile_dir, '*{0}.*'.format(id_tile)))
    rpg_shape_files = [file for file in rpg_shape_files if file[-4:] not in [".gdb", ".xml"]]
    shp_ext = [os.path.splitext(file)[1] for file in rpg_shape_files]
    shp_name = os.path.splitext(os.path.basename(rpg_shape_files[0]))[0]
    logging.info(" rpg shape name: {0}".format(shp_name))
    logging.info(" rpg ext list: {0}".format(shp_ext))
    region_shape_files = glob.glob(os.path.join(rpg_tile_dir, '*region*'.format(id_tile)))
    all_shp_files = rpg_shape_files + region_shape_files

    for file in all_shp_files:
        out_name = os.path.join(out_dir, os.path.basename(file))
        if out_name[-4:] == ".gdb":
            continue
        logging.info(" copy {0} to {1}".format(file, out_name))
        shutil.copyfile(file, out_name)

    # 2 copy asp rpg and rename
    # find the directory containing asp rpg for this tile
    rpg_asp_dir = args.rpg_asp_dir
    rpg_year = args.year
    rpg_asp_files = glob.glob(os.path.join(rpg_asp_dir, '*{0}*{1}*'.format(rpg_year, id_tile)))
    if not rpg_asp_files:
        rpg_asp_tile_dirs = glob.glob(os.path.join(rpg_asp_dir, '*{0}*'.format(id_tile)))
        rpg_asp_tile_dir = rpg_tile_dirs[0]
        rpg_asp_files = glob.glob(os.path.join(rpg_asp_tile_dir, '*asp*'))

    rpg_asp_shp = None
    for file in rpg_asp_files:
        base_without_ext = os.path.splitext(os.path.basename(file))[0]
        ext = os.path.splitext(file)[1]
        if base_without_ext.find("asp") != -1:
            out_name = os.path.join(out_dir, base_without_ext + ext)
        else:
            out_name = os.path.join(out_dir, base_without_ext+"_asp"+ext)

        logging.info(" copy {0} to {1}".format(file, out_name))
        shutil.copyfile(file, out_name)
        if ext == ".shp":
            rpg_asp_shp = out_name

    logging.info(" rpg_asp_shp : {0}".format(rpg_asp_shp))

    # 3 compute rpg mask with georef of first classification
    classif_base = os.path.join(tile_classifs_list[0], "tmp", "final", "Classif_Seed_0.tif")
    if not os.path.exists(classif_base):
        logging.error("base classif  {0} doesn't exists".format(classif_base))
        sys.exit(0)
    logging.info(" classif_base {0}".format(classif_base))
    in_rpg_shp = os.path.join(out_dir, shp_name+".shp")
    logging.info(" in rpg for mask {0}".format(in_rpg_shp))
    out_rpg_mask = os.path.join(out_dir, shp_name+"_mask"+".tif")
    logging.info(" out_rpg_mask {0}".format(out_rpg_mask))
    compute_mask(in_rpg_shp, classif_base, out_rpg_mask)

    # 4 copy classif, mask by rpg
    out_classif_list = []
    for classif in tile_classifs_list:
        out_results = copy_classif_final(classif, out_dir)
        out_classif_list.append(out_results[0])
        mask_classif(out_results[0], out_rpg_mask)

        # classif_base = os.path.join(classif, "tmp", "final", "Classif_Seed_0.tif")
        # suffix_dict = {
            # "LBA_05": "N17_Avril",
            # "LBA_07": "N17_Juillet",
            # "LBA_11": "N17_Novembre",
            # "LBB_05": "N43_Avril",
            # "LBB_07": "N43_Juillet",
            # "LBB_11": "N43_Novembre"
        # }

        # in_dirname = os.path.basename(classif)
        # in_suffix = "_".join(in_dirname.split("_")[4:])
        # logging.info(" copy classif in_suffix = {0}".format(in_suffix))
        # if in_suffix in suffix_dict:
            # out_suffix = suffix_dict[in_suffix]
        # else:
            # out_suffix = in_suffix
        # logging.info(" copy classif out_suffix = {0}".format(out_suffix))
        # out_classif = os.path.join(out_dir, "Classif_" + out_suffix + ".tif")
        # out_classif_list.append(out_classif)

    # 5 copy rpg data
    result_rpg_shp = os.path.join(out_dir, shp_name+"_results"+".shp")
    remove_fields = ["LABIGN", "LABPAC", "LABELA", "LABELB", "TEMPORAL"]
    rename_fields = {"LABELBA": "N17", "LABELBB": "N43"}
    logging.info(" copy rpg to {0}".format(result_rpg_shp))

    export_rpg_shapefile(in_rpg_shp, result_rpg_shp, remove_fields, rename_fields)

    # 6 add field from classif zonal stat
    for result_classif in out_classif_list:
        classif_basename = os.path.splitext(os.path.basename(result_classif))[0]
        classif_suffix = "_".join(classif_basename.split("_")[1:])
        logging.info("field stat classif suffix {0}".format(classif_suffix))
        add_classif_parcel_stat(result_rpg_shp, result_classif, fields_suffix=classif_suffix.upper())

    # 7 add code pacage from asp rpg
    logging.info(" add code pacage".format(result_rpg_shp))
    add_join_field(result_rpg_shp, rpg_asp_shp, join_field="PACAGE")

