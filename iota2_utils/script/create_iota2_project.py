#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging

import os
import fiona
import datetime
import json
import shutil
from string import Template


def get_tiles_feature(tile_shapefile, select_tile_list=None, tile_id_field="Name"):
    """
    return liste of tile features from tile shapefile and a liste of tile id

    :param tile_shapefile: shapefile ith tile enveloppes (world or country)
    :param select_tile_list: list of tile if from which retourning tile feature
    :return: list of geojson feature
    """
    out_tiles = []
    select_tile_names = []
    for tile in select_tile_list:
        if tile[0] != "T":
            select_tile_names.append("T{0}".format(tile))
        else:
            select_tile_names.append(tile)

    with fiona.open(tile_shapefile, 'r') as layer_tile:

        if select_tile_list is not None:
            for feat in layer_tile:
                curr_tile_id = feat['properties'][tile_id_field]
                # logging.info("tile_id {0}".format(curr_tile_id))
                if curr_tile_id[0] != "T":
                    curr_tile_id = "T{0}".format(curr_tile_id)

                if curr_tile_id in select_tile_names:
                    logging.info("add feature {0}".format(curr_tile_id))
                    out_tiles.append(feat)
        else:
            for feat in layer_tile:
                out_tiles.append(feat)

    return out_tiles


def get_classif_period_list(periode_dates_string_list):
    """
    return liste of begin/end date from string of type "('yyyymmdd','yyyymmdd')"

    couple (begin, end) is delimited by parenthese and date string format is yyyymmdd

    :param periode_dates_string_list:
    :return: list of tuple en begin, end dates
    """
    interval_dates_list = []
    for periode in periode_dates_string_list:
        date_list_str = periode.split(",")
        if len(date_list_str) != 2:
            raise ValueError("date period should only contain two dates, begin date and end date")

        begin_str = date_list_str[0]
        end_str = date_list_str[1]
        for ch in ["'", '"', '(', ')', " "]:
            begin_str = begin_str.replace(ch, "")
            end_str = end_str.replace(ch, "")
        # logging.info("period ({0}, {1})".format(begin_str, end_str))
        begin_date = datetime.datetime.strptime(begin_str, "%Y%m%d")
        end_date = datetime.datetime.strptime(end_str, "%Y%m%d")
        interval_dates_list.append((begin_date, end_date))
    return interval_dates_list


def get_nomenclature_config(nomenclature_prefix_list, nomenclature_root_dir):
    """
    load nomenclature dictionnary config from nomenclature prefix and root dir containing nomenclature json config file.

    :param nomenclature_prefix_list:
    :param nomenclature_root_dir:
    :return:
    """
    nomenclatures_dict = dict()
    for prefix in nomenclature_prefix_list:
        nomenclature_config_path = os.path.join(
            os.path.abspath(nomenclature_root_dir), "nomenclature_{0}.json".format(prefix))
        if not os.path.exists(nomenclature_config_path):
            raise ValueError("nomenclature file {0} doesn't exists".format(nomenclature_config_path))

        with open(nomenclature_config_path, "r") as config_json:
            nomenclature_dict = json.load(config_json)

        nomenclatures_dict[prefix] = nomenclature_dict

    return nomenclatures_dict


def iota2_create_project(project_path):
    """
    create empty tree for iota2 classification project

    :param project_path:
    :return:
    """
    if not os.path.exists(project_path):
        os.makedirs(project_path)

    iota2_subdir = ["config", "data", "reference", "feature", "log"]
    for subdir in iota2_subdir:
        if not os.path.exists(os.path.join(project_path, subdir)):
            os.makedirs(os.path.join(project_path, subdir))

    if not  os.path.exists(os.path.join(project_path, "data", "jobs")):
        os.makedirs(os.path.join(project_path, "data", "jobs"))


def iota2_config_rpg_from_template(project_path, iota2_template_dir, tile_id, nomenclature_conf, period):
    """

    :param project_path:
    :param iota2_config:
    :param iota2_SAR_config:
    :return:
    """

    template_dict = dict()
    if tile_id[0] == "T":
        template_dict['tile_shortname']= tile_id[1:]
        template_dict['tile_id'] = tile_id
    else:
        template_dict['tile_shortname'] = tile_id
        template_dict['tile_id'] = "T{0}".format(tile_id)

    template_dict['out_classif_dir'] = os.path.basename(project_path)
    template_dict['begindate'] = period[0].strftime("%Y%m%d")
    template_dict['enddate'] = period[1].strftime("%Y%m%d")
    template_dict['nomenclature_label'] = nomenclature_conf['nomenclature_label']
    template_dict['nomenclature_sample'] = nomenclature_conf['nomenclature_sample']
    template_dict['nomenclature_field'] = nomenclature_conf['nomenclature_field']
    template_dict['nomenclature_color'] = nomenclature_conf['nomenclature_color']

    sar_config_template = os.path.join(iota2_template_dir, "SARconfig_template.cfg")
    iota2_config_template = os.path.join(iota2_template_dir, "Config_template.cfg")

    # configure iota2
    iota2_template = open(iota2_config_template,"r")
    iota2_conf_src = Template(iota2_template.read())
    iota2_conf_result = iota2_conf_src.substitute(template_dict)
    out_conf_file = open(os.path.join(project_path, "config", "Config.cfg"), 'w')
    out_conf_file.write(iota2_conf_result)
    out_conf_file.close()
    iota2_template.close()

    # configure sar
    sar_template = open(sar_config_template, "r")
    sar_conf_src = Template(sar_template.read() )
    sar_conf_result = sar_conf_src.substitute(template_dict)
    out_sar_conf_file = open(os.path.join(project_path, "config", "SARconfig.cfg"), 'w')
    out_sar_conf_file.write(sar_conf_result)
    out_sar_conf_file.close()
    sar_template.close()


def iota2_update_region_from_tile(
        project_path, tile_feature, region_field_name="regionstr", region_crs=None, tile_id_field="Name"):
    """
    create region shapefile form tile envelope

    :param project_path:
    :param feature:
    :param region_field_name:
    :return:
    """
    region_driver = 'ESRI Shapefile'
    region_crs = region_crs
    region_schema = {
        'geometry': 'Polygon',
        'properties': {
            'region': 'int',
            region_field_name: 'str',
            'tile': 'str',
            'MC': 'int'
        }
    }

    # export region
    region_filename = os.path.abspath(os.path.join(project_path, "region.shp"))

    with fiona.open(region_filename, 'w', driver=region_driver, crs=region_crs, schema=region_schema,
                    encoding='utf-8') as out_region:
        out_feature = dict()
        out_feature["geometry"] = tile_feat["geometry"]
        out_feature["properties"] = dict()
        out_feature["properties"]["region"] = 1
        out_feature["properties"][region_field_name] = "1"
        out_feature["properties"]['tile'] = tile_feat["properties"][tile_id_field]
        out_feature["properties"]['MC'] = 1
        out_region.write(out_feature)


def iota2_config_rpg_copy_data(
        project_path, iota2_template_dir, tile_id, nomenclature_conf, nomenclature_id, rpg_data_dir):
    """

    :param project_path:
    :param iota2_config:
    :param iota2_SAR_config:
    :return:
    """
    if tile_id[0] == "T":
        tile_shortname = tile_id[1:]
        tile_name = tile_id
    else:
        tile_shortname = tile_id
        tile_name = "T{0}".format(tile_id)

    in_nomenclature_sample = os.path.join(
        rpg_data_dir, tile_name, "rpg_2017_{0}_{1}".format(tile_name, nomenclature_conf["nomenclature_sample"]))
    out_nomenclature_sample = os.path.join(
        project_path, "reference", "rpg_2017_{0}_{1}".format(tile_name, nomenclature_conf["nomenclature_sample"]))
    in_nomenclature_color = os.path.join(iota2_template_dir, nomenclature_conf['nomenclature_color'])
    out_nomenclature_color = os.path.join(project_path, "reference", nomenclature_conf['nomenclature_color'])

    in_nomenclature_label = os.path.join(iota2_template_dir, nomenclature_conf['nomenclature_label'])
    out_nomenclature_label = os.path.join(project_path, "reference", nomenclature_conf['nomenclature_label'])

    shutil.copyfile(in_nomenclature_color, out_nomenclature_color)
    shutil.copyfile(in_nomenclature_label, out_nomenclature_label)
    for ext in [".cpg", ".dbf", ".shx", ".shp", ".prj", ".sbn", "sbx"]:
        if os.path.exists(in_nomenclature_sample+ext):
            shutil.copyfile(in_nomenclature_sample+ext, out_nomenclature_sample+ext)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="post process classifications")

    parser.add_argument(
        "-tl", "--tile_list", nargs='+',
        dest="tile_list", help="list of s2 tiles from which iota2 project will be created", required=False)
    parser.add_argument(
        "-ts", "--tile_shp", dest="tile_shp", help="shapefile with s2 Tiles from which iota2 project will be created",
        required=True)
    parser.add_argument(
        "-rp", "--rpg_prep_dir", dest="rpg_prep_dir", help="directory containing rpg preprocessing shapefile by tile",
        required=True)
    parser.add_argument(
        "-i2", "--iota2_template", dest="iota2_template", help="directory with jinja template files for iota2 project",
        required=True)
    parser.add_argument(
        "-p", "--periods", dest="periods",  nargs='+',
        help="list of couple of begin end date for iota2 project ('yyyymmdd', 'yyyymmdd')", required=True)
    parser.add_argument(
        "-n", "--nomenclatures", dest="nomenclatures",  nargs='+',
        help="list of nomenclatures for iota2 project", required=True)
    parser.add_argument(
        "-o", "--out_dir", dest="out_dir", help="out result directory", required=True)
    parser.add_argument(
        "-b", "--out_basename", dest="out_basename", help="string template for output projet names",
        required=False)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    tile_id_field = "Name"
    region_field_name = "regionstr"

    if args.tile_list:
        out_tile_list = args.tile_list
    else:
        out_tile_list = None

    out_root_dir = args.out_dir
    out_crs = None
    with fiona.open(args.tile_shp, 'r') as layer_tile:
        out_crs = layer_tile.crs

    out_tiles_features = get_tiles_feature(args.tile_shp, out_tile_list,  tile_id_field)
    interval_dates_list = get_classif_period_list(args.periods)
    nomenclatures_config = get_nomenclature_config(args.nomenclatures, args.iota2_template)

    # logging.info("out_tiles_features {0}".format(out_tiles_features))
    logging.info("interval_dates_list {0}".format(interval_dates_list))
    logging.info("nomenclatures_config {0}".format(nomenclatures_config))
    logging.info("out_basename {0}".format(args.out_basename))

    out_name_template = Template(args.out_basename)

    for tile_feat in out_tiles_features:
        for nomenclature_id in nomenclatures_config:
            for interval in interval_dates_list:
                tile_id = tile_feat["properties"][tile_id_field]
                end = interval[1].strftime("%B")[0:3].upper()
                template_dict = dict(tile=tile_id, end=end, nomenclature=nomenclature_id)
                logging.info("template_dict {0}".format(template_dict))

                out_name = out_name_template.substitute(template_dict)
                logging.info("create classif project {0}".format(out_name))
                out_project_path = os.path.join(out_root_dir, out_name)
                iota2_create_project(out_project_path)
                iota2_update_region_from_tile(
                    out_project_path, tile_feat, region_field_name=region_field_name, region_crs=out_crs,
                    tile_id_field=tile_id_field)
                iota2_config_rpg_from_template(
                    out_project_path, args.iota2_template, tile_id, nomenclatures_config[nomenclature_id], interval)
                iota2_config_rpg_copy_data(
                    out_project_path, args.iota2_template, tile_id, nomenclatures_config[nomenclature_id],
                    nomenclature_id, args.rpg_prep_dir)
