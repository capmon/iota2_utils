#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
from glob import glob

import os
import pandas as pd
import csv
import numpy as np
import sklearn.metrics as skm
import fiona
from shapely.geometry import shape


label_17 = ['0', '1', '2', '4', '5', '6', '7', '8', '10', '11', '12', '13', '14', '15', '16', '17', '18']
label_43 = ['0', '1', '2', '3', '4', '5', '6', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
            '21', '22', '23', '27', '28', '30', '31', '33', '34', '35', '36', '37', '38', '39', '41', '43']
dict_n17 = {
    '0': 'indef',
    '1': 'herbe_pred',
    '2': 'cereales',
    '4': 'chanvre',
    '5': 'fourrage_legum',
    '6': 'fourrage_autre',
    '7': 'oleagineux',
    '8': 'proteagineux',
    '9': 'riz',
    '10': 'culture_indu',
    '11': 'fruit_leg_fleur',
    '12': 'fleurs_arom_med',
    '13': 'ligneux',
    '14': 'verger',
    '15': 'vignes',
    '16': 'tail_cour_rot',
    '17': 'surf_non_agri',
    '18': 'autres'
}

dict_n43 = {
 '0': 'indef',
 '1': 'herbe_pred',
 '2': 'ble_dur_printemps',
 '3': 'ble_dur_hiver',
 '4': 'cereale_printemps',
 '5': 'cereale_ete',
 '6': 'cereale_hiver',
 '7': 'mais_zea',
 '8': 'sorgho_millet_moha',
 '9': 'autres_cereales',
 '10': 'sarrasin',
 '11': 'chanvre',
 '12': 'fourrage_legum',
 '13': 'fourrage_autre',
 '14': 'colza_hiver',
 '15': 'colza_printemps',
 '16': 'tournesol',
 '17': 'oleagineux_autre',
 '18': 'soja',
 '19': 'proteagineux_autres',
 '20': 'riz',
 '21': 'betterave',
 '22': 'lin',
 '23': 'pomme_de_terre',
 '24': 'tabac',
 '25': 'banane',
 '26': 'cacao',
 '27': 'legume_sec',
 '28': 'fruit_leg_fleur_autre',
 '29': 'lavande',
 '30': 'fleurs_arom_med_autres',
 '31': 'ligneux',
 '32': 'oliveraies',
 '33': 'autres_verger',
 '34': 'vignes',
 '35': 'tail_cour_rot',
 '36': 'surf_non_agri',
 '37': 'bati',
 '38': 'biomasse',
 '39': 'bois',
 '40': 'canne_sucre',
 '41': 'houblon',
 '42': 'eaux_marais',
 '43': 'autres'
}


def load_stat_df_from_shp(rpg_file, nomenclature="N17", with_code_cultu="CODE_CULTU"):

    surf_ha = []
    label_true = []
    pred_may = []
    pred_aug = []
    pred_nov = []
    if with_code_cultu:
        code_cultu = []

    with fiona.open(rpg_file) as rpg_shp:
        for feat in rpg_shp:
            curr_label_true = int(feat["properties"][nomenclature])
            curr_pred_nov = int(feat["properties"]["L1_{0}_NOV".format(nomenclature)])
            curr_pred_aug = int(feat["properties"]["L1_{0}_JUI".format(nomenclature)])
            curr_pred_may = int(feat["properties"]["L1_{0}_AVR".format(nomenclature)])
            f_shape = shape(feat['geometry'])
            ha = f_shape.area / 10000.0
            label_true.append(curr_label_true)
            surf_ha.append(ha)
            pred_nov.append(curr_pred_nov)
            pred_aug.append(curr_pred_aug)
            pred_may.append(curr_pred_may)
            if with_code_cultu:
                curr_code_cultu = feat["properties"]["CODE_CULTU"]
                code_cultu.append(curr_code_cultu)

    df_id = [i for i in range(0, len(label_true))]
    logging.info("len label {1} {0}".format(len(label_17), nomenclature))
    df_parcel = pd.DataFrame(
        data={nomenclature: label_true,
              "aire": surf_ha,
              "pred_may": pred_may,
              "pred_aug": pred_aug,
              "pred_nov": pred_nov,
              "code_cultu": code_cultu
              },
        index=df_id)
    return df_parcel


def confusion_matrix(y_true, y_pred, label, label_names, margin=True, normalize=False):
    """

    :param y_true:
    :param y_pred:
    :param label:
    :param margin:
    :param normalize:
    :return:
    """
    df_confusion = pd.crosstab(
        y_true, y_pred, rownames=[u'Déclaré'], colnames=['Prediction'], margins=margin, normalize=normalize)
    # logging.info("df_confusion index \n{0}".format(df_confusion.index))
    label_conf = label.copy()
    if margin:
        label_conf.append("All")
    # logging.info("label_conf \n{0}".format(label_conf))

    df_confusion = df_confusion.reindex(label_conf).fillna(0)
    df_confusion = df_confusion.reindex(label_conf, axis="columns").fillna(0)
    names_conf = label_names.copy()
    if margin:
        names_conf.append("All")
    df_confusion.index = names_conf
    df_confusion.columns = names_conf
    return df_confusion


def confusion_matrix_by_code(
        df, col_true, col_pred, col_code, label, label_names, label_cultu, multi_index):
    """

    """

    gb = df.groupby([col_true, col_code])
    stat_gb = gb['aire'].agg([np.sum, len]).rename(columns={'sum': 'surf_tot', 'len': 'nb_parcelle'})
    stat_gb = stat_gb.reindex(multi_index).fillna(0)
    # logging.info("stat_gb \n{0}".format(stat_gb))

    df_confusion = pd.crosstab(
        [df[col_true], df[col_code]], df[col_pred], rownames=[u'label', 'code_cultu'], colnames=['Prediction'])
    df_confusion_norm = pd.crosstab(
        [df[col_true], df[col_code]], df[col_pred], rownames=[u'label', 'code_cultu'], colnames=['Prediction'],
        normalize='index')

    # reindex with all possible value
    df_confusion = df_confusion.reindex(label, axis="columns").fillna(0)
    df_confusion = df_confusion.reindex(multi_index).fillna(0)
    df_confusion.columns = label_names
    df_confusion_norm = df_confusion_norm.reindex(label, axis="columns").fillna(0)
    df_confusion_norm = df_confusion_norm.reindex(multi_index).fillna(0)
    df_confusion_norm.columns = ["%{0}".format(name) for name in label_names]

    df_confusion = pd.concat([stat_gb, df_confusion_norm, df_confusion], axis=1)

    # add column with code_cultu label
    code_cultu = [x[1] for x in df_confusion_norm.index]
    # logging.info("code_cultu \n{0}".format(code_cultu))
    label_cultu_col = [label_cultu[code] for code in code_cultu]
    df_confusion["label_cultu"] = label_cultu_col

    # reorder columns
    cols = df_confusion.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    df_confusion = df_confusion[cols]

    # sort by index
    df_confusion.sort_index(inplace=True)

    # renamne index from code to label
    label_dict = dict(zip(label, label_names))
    # logging.info("label_dict \n{0}".format(label_dict))
    df_confusion.index = pd.MultiIndex.from_tuples(
        [(label_dict[x[0]], x[1]) for x in df_confusion.index])

    return df_confusion


def compute_df_stat_by_nomenclature(
        df_parcel, out_stat_file, nomenclature="N17", label_dict=dict_n17, out_stat_csv=None, out_stat_xls= None,
        label_cultu_dict=None, multi_index_cultu=None):
    """

    :param rpg_file:
    :param nomenclature:
    :return:
    """
    aire_cat_list = [("[0.00 0.25[", 0),
                     ("[0.25 0.50[", 0.25),
                     ("[0.50 1.00[", 0.5),
                     ("[1.00 inf[", 1)]

    surf_ha = df_parcel["aire"]
    label_true = df_parcel[nomenclature]
    pred_may = df_parcel["pred_may"]
    pred_aug = df_parcel["pred_aug"]
    pred_nov = df_parcel["pred_nov"]

    label = [int(k) for k in label_dict.keys()]
    label.sort()
    logging.info("label \n{0}".format(label))
    label_names = [label_dict[str(l)] for l in label]

    # calcul stat aire/nb parcelle sur données rpg de la tuile
    gb = df_parcel.groupby(nomenclature)
    stat_gb = gb['aire'].agg([np.sum, len]).rename(columns={'sum': 'surf_tot', 'len': 'nb_parcelle'})
    logging.info("stat_gb \n{0}".format(stat_gb))

    # calcul stat precision/rappel/f1_score par classe de la nomenclature
    multi_index = pd.MultiIndex.from_product(
        [["Iota2_AVR", "Iota2_JUI", "Iota2_NOV"], ["Precision %", "Rappel %", "F_score %"]], names=['first', 'second'])
    df = pd.DataFrame(np.zeros((len(label), 9)), index=label, columns=multi_index)
    logging.info("df_ini \n{0}".format(df))

    for pred in [("NOV", pred_nov),
                 ("JUI", pred_aug),
                 ("AVR",pred_may)]:

        precisions, recalls, f1_scores, supports = skm.precision_recall_fscore_support(
            label_true, pred[1], average=None, labels=label)
        logging.info("len precisions {1} {0}".format(len(precisions), nomenclature))

        df["Iota2_{0}".format(pred[0]), "Precision %"] = precisions*100.0
        df["Iota2_{0}".format(pred[0]), "Rappel %"] = recalls*100.0
        df["Iota2_{0}".format(pred[0]), "F_score %"] = f1_scores*100.0

    stat_gb = stat_gb.reindex(df.index).fillna(0)
    logging.info("stat_gb \n{0}".format(stat_gb))
    logging.info("stat_gb['surf_tot'] \n{0}".format(stat_gb['surf_tot']))
    df["RPG", "surface ha"] = stat_gb['surf_tot'].values
    df["RPG", "nb parcelle"] = stat_gb['nb_parcelle'].values
    df["label"] = label_names

    logging.info("df_end \n{0}".format(df))

    df.to_csv(
        out_stat_csv, sep='\t', encoding='utf-8', float_format='%.2f')
    df.to_excel(
        out_stat_xls, sheet_name='score_by_class', encoding='utf-8', float_format='%.3f')
    out_stat_file.write(" stat {0} classes \n".format(nomenclature))
    out_stat_file.write("{0}".format(df))

    # calcul stat precision/rappel/f1_score globaux
    out_stat_file.write("\n\t OA \t F-Score   \n")
    for pred in [("NOV", pred_nov),
                 ("JUI", pred_aug),
                 ("AVR",pred_may)]:
        OA_pred = skm.accuracy_score(label_true, pred[1], normalize=True)
        # logging.info("{1}_OA_{2} {0}".format(n17_OA_nov))
        F1_pred = skm.f1_score(label_true,  pred[1], labels=label, average="macro")
        # logging.info("{1}_F1_nov {0}".format(n17_F1_nov))
        out_stat_file.write("{2} \t {0}\t {1}\n".format(OA_pred, F1_pred, pred[0]))

    # calcul matrice confusion
    for pred in [("NOV", pred_nov),
                 ("JUI", pred_aug),
                 ("AVR", pred_may)]:
        df_confusion = confusion_matrix(label_true, pred[1], label, label_names, margin=True, normalize=False)
        df_confusion.to_excel(
            out_stat_xls, sheet_name='confusion_matrix_{0}'.format(pred[0]), encoding='utf-8', float_format='%.3f')

    # calcul matrice confusion
    for pred in [("NOV", pred_nov),
                 ("JUI", pred_aug),
                 ("AVR", pred_may)]:
        df_confusion = confusion_matrix(label_true, pred[1], label, label_names, margin=True, normalize='index')
        df_confusion.to_excel(
            out_stat_xls, sheet_name='confusion_matrix_norm_{0}'.format(pred[0]), encoding='utf-8', float_format='%.3f')

    df_conf_dict = dict()
    for i in range(0, len(aire_cat_list)):
        # print("i = {0}".format(i))
        if i == 0:
            max_filter_value = aire_cat_list[i+1][1]
            df_cat = df_parcel[df_parcel["aire"] <= max_filter_value]
        elif i == (len(aire_cat_list)-1):
            min_filter_value = aire_cat_list[i][1]
            df_cat = df_parcel[df_parcel["aire"] > min_filter_value]
        else:
            min_filter_value = aire_cat_list[i][1]
            max_filter_value = aire_cat_list[i+1][1]
            df_cat = df_parcel[(df_parcel["aire"] > min_filter_value) & (df_parcel["aire"] <= max_filter_value)]

        df_conf_cat = confusion_matrix_by_code(
            df_cat, nomenclature, "pred_nov", "code_cultu", label, label_names, label_cultu_dict, multi_index_cultu)
        df_conf_dict[aire_cat_list[i][0]] = df_conf_cat

    df_conf_code_all = confusion_matrix_by_code(
        df_parcel, nomenclature, "pred_nov", "code_cultu", label, label_names, label_cultu_dict, multi_index_cultu)
    df_conf_dict["all"] = df_conf_code_all
    df_conf_code_jui = pd.concat(df_conf_dict.values(), keys=df_conf_dict.keys())
    df_conf_code_jui = df_conf_code_jui.reorder_levels([1, 2, 0]).sort_index()
    df_conf_code_jui = df_conf_code_jui.reindex(label_names, level=0)

    p_names = ["%{0}".format(name) for name in label_names]

    def row_argsort(x):
        sort_row = x.sort_values(ascending=False)
        # logging.info("row {0}\n argsort:\n {1}".format(x.name, sort_row[0:3]))
        if sort_row.iloc[0] == 0.0:
            precision = np.NaN
            argmax = ""
            max_conf_label = ""
            max_conf_value = np.NaN
        else:
            argmax = sort_row.index[0][1:]
            row_code = x.name[0]
            if argmax == row_code:
                max_conf_label = sort_row.index[1][1:]
                max_conf_value = sort_row.iloc[1]
            else:
                max_conf_label = sort_row.index[0][1:]
                max_conf_value = sort_row.iloc[0]
            precision = x["%{0}".format(row_code)]
        return pd.Series(
            [precision, argmax, max_conf_label, max_conf_value],
            index=["precision", "argmax", "max_conf_label", "max_conf_value"])

    df_new_col = df_conf_code_jui[p_names].apply(row_argsort, axis=1)
    df_conf_code_jui = pd.concat([df_new_col, df_conf_code_jui], axis=1)
    # logging.info("df_new_col \n{0}".format(df_new_col))

    df_conf_code_jui.to_excel(
            out_stat_xls, sheet_name='confusion_matrix_code_JUI', encoding='utf-8', float_format='%.3f')


def load_rpg_nomenclature(rpg_nomenclature_csv):
    """

    :param rpg_nomenclature_csv:
    :return:
    """
    label_cultu = dict()
    code_exp_asp_17_dict = dict()
    code_exp_asp_43_dict = dict()

    if rpg_nomenclature_csv is not None:
        nomenclature_file = open(rpg_nomenclature_csv, "r")
        nomenclature_csv_reader = csv.reader(nomenclature_file, delimiter=";")
        nomenclature_csv_header = next(nomenclature_csv_reader)
        for ligne in nomenclature_csv_reader:
            code_cultu = ligne[0]
            label_cultu[code_cultu] = ligne[1]
            code_exp_asp_17_dict[code_cultu] = int(ligne[6])
            code_exp_asp_43_dict[code_cultu] = int(ligne[8])
        nomenclature_file.close()

    tuples_17 = [(code_exp_asp_17_dict[k], k) for k in code_exp_asp_17_dict]
    multi_index_17 = pd.MultiIndex.from_tuples(tuples_17, names=['N17', 'code_cultu'])

    tuples_43 = [(code_exp_asp_43_dict[k], k) for k in code_exp_asp_43_dict]
    multi_index_43 = pd.MultiIndex.from_tuples(tuples_43, names=['N43', 'code_cultu'])

    return label_cultu, multi_index_17, multi_index_43


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="post process compute stats")

    parser.add_argument("-i", "--in_classif_dir", dest="classif_dir",
                        help="root directory containing iota2 classification result", required=True)
    parser.add_argument("-t", "--id_tile", dest="id_tile", nargs='*', help="ids list of the S2 tile to postprocess",
                        required=True)
    parser.add_argument("-y", "--year", dest="year", help="year of classification and rpg data", required=True)
    parser.add_argument("-o", "--out_dir", dest="out_dir", help="out result directory",
                        required=True)
    parser.add_argument("-c", "--code_rpg", dest="code_rpg", help="fichier csv avec liste /label des codes culture rpg",
                        required=True)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    out_root_dir = args.out_dir

    # first get list of tile id
    logging.info("tile to postprocess : {id_tile_list}".format(id_tile_list=args.id_tile))
    id_tile_list = [id.upper() for id in args.id_tile]
    if len(id_tile_list) == 1:
        logging.info("tile len 1 : {0}".format(id_tile_list[0]))
        if id_tile_list[0] == "*":
            logging.info("search all tile")
            abs_root_path = os.path.abspath(args.classif_dir)
            logging.info("abs_root_path {0}".format(abs_root_path))

            tile_subdirs = glob(abs_root_path+'/T[0-9][0-9]*/')
            logging.info("tile_subdirs : {0}".format(tile_subdirs))

            id_tile_list = [os.path.basename(os.path.dirname(subdir)) for subdir in tile_subdirs]

    logging.info("id_tile_list : {0}".format(id_tile_list))
    logging.info("len(id_tile_list) : {0}".format(len(id_tile_list)))

    # load nomenclature and mapping dict
    label_cultu, multi_index_17, multi_index_43 = load_rpg_nomenclature(args.code_rpg)

    df_parcel_17_all = None
    df_parcel_43_all = None

    df = pd.DataFrame({'Data': [10, 20, 30, 20, 15, 30, 45]})
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer_test = pd.ExcelWriter(os.path.join(args.classif_dir,'pandas_simple.xlsx'), engine='xlsxwriter')
    # Convert the dataframe to an XlsxWriter Excel object.
    df.to_excel(writer_test, sheet_name='Sheet1')
    # Close the Pandas Excel writer and output the Excel file.
    writer_test.save()

    for id_tile in id_tile_list:

        curr_out_dir = os.path.join(out_root_dir, id_tile)
        if not os.path.exists(curr_out_dir):
            os.makedirs(curr_out_dir)
        # get rpg result filename
        rpg_filename = os.path.join(args.classif_dir, id_tile, "rpg_{0}_{1}_results.shp".format(args.year, id_tile))
        logging.info("rpg result filename : {rpg_filename}".format(rpg_filename=rpg_filename))
        if not os.path.exists(rpg_filename):
            logging.info("rpg result filename not exist")
            exit(1)

        # init out report file
        out_stat_report = os.path.join(curr_out_dir, "stat_{0}".format(id_tile) + ".txt")
        out_stat_file = open(out_stat_report, "w")

        # compute stat
        df_parcel_17 = load_stat_df_from_shp(rpg_filename, nomenclature="N17")
        out_stat_csv_17 = os.path.join(curr_out_dir, "stat_{0}_n17".format(id_tile) + ".csv")
        out_stat_xls_17 = os.path.join(curr_out_dir, "stat_{0}_n17".format(id_tile) + ".xlsx")
        with pd.ExcelWriter(out_stat_xls_17, engine='xlsxwriter') as xls_writer_17:
            compute_df_stat_by_nomenclature(
                df_parcel_17,  out_stat_file, nomenclature="N17", label_dict=dict_n17, out_stat_csv=out_stat_csv_17,
                out_stat_xls=xls_writer_17, label_cultu_dict=label_cultu, multi_index_cultu=multi_index_17)
            logging.info("xls_writer_17 save")
            # xls_writer_17.save()

        logging.info("load df 43")
        df_parcel_43 = load_stat_df_from_shp(rpg_filename, nomenclature="N43")
        out_stat_csv_43 = os.path.join(curr_out_dir, "stat_{0}_n43".format(id_tile) + ".csv")
        out_stat_xls_43 = os.path.join(curr_out_dir, "stat_{0}_n43".format(id_tile) + ".xlsx")
        xls_writer_43 = pd.ExcelWriter(out_stat_xls_43, engine='xlsxwriter')
        compute_df_stat_by_nomenclature(
            df_parcel_43,  out_stat_file, nomenclature="N43", label_dict=dict_n43, out_stat_csv=out_stat_csv_43,
            out_stat_xls=xls_writer_43, label_cultu_dict=label_cultu, multi_index_cultu=multi_index_43)
        logging.info("xls_writer_43 save")
        xls_writer_43.save()

        out_stat_file.close()

        # append parcel to global dataframe
        if df_parcel_17_all is None:
            df_parcel_17_all = df_parcel_17
        else:
            df_parcel_17_all = pd.concat([df_parcel_17_all, df_parcel_17])

        if df_parcel_43_all is None:
            df_parcel_43_all = df_parcel_43
        else:
            df_parcel_43_all = pd.concat([df_parcel_43_all, df_parcel_43])

    # global stat
    # init out report file
    out_stat_report = os.path.join(out_root_dir, "stat_all" + ".txt")
    out_stat_file = open(out_stat_report, "w")

    # compute stat
    out_stat_csv_17 = os.path.join(out_root_dir, "stat_all_n17".format(id_tile) + ".csv")
    out_stat_xls_17 = os.path.join(out_root_dir, "stat_all_n17".format(id_tile) + ".xls")
    xls_writer_17 = pd.ExcelWriter(out_stat_xls_17, engine='xlsxwriter')
    compute_df_stat_by_nomenclature(
        df_parcel_17_all,  out_stat_file, nomenclature="N17", label_dict=dict_n17, out_stat_csv=out_stat_csv_17,
        out_stat_xls=xls_writer_17, label_cultu_dict=label_cultu, multi_index_cultu=multi_index_17)
    xls_writer_17.save()

    out_stat_csv_43 = os.path.join(out_root_dir, "stat_all_n43".format(id_tile) + ".csv")
    out_stat_xls_43 = os.path.join(out_root_dir, "stat_all_n43".format(id_tile) + ".xls")
    xls_writer_43 = pd.ExcelWriter(out_stat_xls_43, engine='xlsxwriter')
    compute_df_stat_by_nomenclature(
        df_parcel_43_all,  out_stat_file, nomenclature="N43", label_dict=dict_n43, out_stat_csv=out_stat_csv_43,
        out_stat_xls=xls_writer_43, label_cultu_dict=label_cultu, multi_index_cultu=multi_index_43)
    xls_writer_43.save()

    out_stat_file.close()

