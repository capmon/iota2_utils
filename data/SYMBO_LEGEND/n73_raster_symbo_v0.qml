<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" version="3.4.6-Madeira" minScale="1e+08" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer classificationMax="73" opacity="0.648" type="singlebandpseudocolor" alphaBand="-1" band="1" classificationMin="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="EXACT" classificationMode="2" clip="0">
          <colorramp name="[source]" type="gradient">
            <prop k="color1" v="166,97,26,255"/>
            <prop k="color2" v="1,133,113,255"/>
            <prop k="discrete" v="0"/>
            <prop k="rampType" v="gradient"/>
            <prop k="stops" v="0.25;223,194,125,255:0.5;245,245,245,255:0.75;128,205,193,255"/>
          </colorramp>
          <item color="#4ed066" alpha="255" label="HERB_PRED" value="1"/>
          <item color="#e8d369" alpha="255" label="BLE_TEN_H" value="2"/>
          <item color="#ffff73" alpha="255" label="MAIS" value="3"/>
          <item color="#ce4732" alpha="255" label="ORGE_H" value="4"/>
          <item color="#ffbee8" alpha="255" label="COLZA_H" value="5"/>
          <item color="#ffbd3e" alpha="255" label="ORGE_P" value="6"/>
          <item color="#a87000" alpha="255" label="LIGN_PRED" value="7"/>
          <item color="#cd6699" alpha="255" label="TOURNESOL" value="8"/>
          <item color="#8400a8" alpha="255" label="VIGNE" value="9"/>
          <item color="#7c00f8" alpha="255" label="BETTERAVE" value="10"/>
          <item color="#42ba9e" alpha="255" label="LUZERNE" value="11"/>
          <item color="#cfc991" alpha="255" label="TRITICA_H" value="12"/>
          <item color="#266018" alpha="255" label="BOIS_PATU" value="13"/>
          <item color="#d98545" alpha="255" label="BLE_DUR_H" value="14"/>
          <item color="#bee8ff" alpha="255" label="FR_LE_FLE" value="15"/>
          <item color="#ffa77f" alpha="255" label="POMM_TERR" value="16"/>
          <item color="#00e6a9" alpha="255" label="LEGU_FOUR" value="17"/>
          <item color="#868686" alpha="255" label="SURF_TNEX" value="18"/>
          <item color="#4c0073" alpha="255" label="SOJA" value="19"/>
          <item color="#3f821d" alpha="255" label="VERGER" value="20"/>
          <item color="#ffd4be" alpha="255" label="LIN_FIBRE" value="21"/>
          <item color="#d7d55b" alpha="255" label="ML_CEREAL" value="22"/>
          <item color="#b9ff01" alpha="255" label="POIS_P" value="23"/>
          <item color="#d7d79e" alpha="255" label="SORGHO" value="24"/>
          <item color="#cd1500" alpha="255" label="BORDURE" value="25"/>
          <item color="#adf6e5" alpha="255" label="FEVEROLE" value="26"/>
          <item color="#745f32" alpha="255" label="CHEN_CHAT" value="27"/>
          <item color="#73ffdf" alpha="255" label="AUTR_FOUR" value="28"/>
          <item color="#000000" alpha="255" label="AVOINE_H" value="29"/>
          <item color="#84da0c" alpha="255" label="POIS_H" value="30"/>
          <item color="#52eec5" alpha="255" label="ML_PR_CR" value="31"/>
          <item color="#91522d" alpha="255" label="CANN_SUCR" value="32"/>
          <item color="#f7f5cd" alpha="255" label="AVOINE_P" value="33"/>
          <item color="#ffa901" alpha="255" label="LENTILLE" value="34"/>
          <item color="#c7c6b0" alpha="255" label="POIS_CHIC" value="35"/>
          <item color="#e6e600" alpha="255" label="SARRASIN" value="36"/>
          <item color="#ccce3f" alpha="255" label="SEIGLE_H" value="37"/>
          <item color="#4f411f" alpha="255" label="NOYERS" value="38"/>
          <item color="#e8beff" alpha="255" label="LAVANDIN" value="39"/>
          <item color="#ffcf75" alpha="255" label="MILLET" value="40"/>
          <item color="#00a9e6" alpha="255" label="PLANT_PAM" value="41"/>
          <item color="#eaf7ce" alpha="255" label="BLE_TEN_P" value="42"/>
          <item color="#ff0000" alpha="255" label="AUTRES" value="43"/>
          <item color="#b7ce51" alpha="255" label="EPEAUTRE" value="44"/>
          <item color="#e1e1e1" alpha="255" label="RIZ" value="45"/>
          <item color="#ffffd5" alpha="255" label="CHANVRE" value="46"/>
          <item color="#8c8c0f" alpha="255" label="OLIVERAIE" value="47"/>
          <item color="#e1c4b6" alpha="255" label="LIN_H" value="48"/>
          <item color="#7c5f2a" alpha="255" label="SURF_BOIS" value="49"/>
          <item color="#eba382" alpha="255" label="LIN_P" value="50"/>
          <item color="#ebe041" alpha="255" label="MOUTARDE" value="51"/>
          <item color="#e57321" alpha="255" label="BLE_DUR_P" value="52"/>
          <item color="#edff8a" alpha="255" label="BANANE" value="53"/>
          <item color="#d79e9e" alpha="255" label="BIOMASSE" value="54"/>
          <item color="#ff73df" alpha="255" label="AUTR_OLEP" value="55"/>
          <item color="#bba7e7" alpha="255" label="TRUFFIERE" value="56"/>
          <item color="#ffb731" alpha="255" label="MOHA" value="57"/>
          <item color="#cdf27a" alpha="255" label="TAIL_CRT" value="58"/>
          <item color="#e9e256" alpha="255" label="CEREALES" value="59"/>
          <item color="#bed2ff" alpha="255" label="EAU_MARAI" value="60"/>
          <item color="#d5ca62" alpha="255" label="TRITICA_P" value="61"/>
          <item color="#f57ab6" alpha="255" label="COLZA_P" value="62"/>
          <item color="#99937c" alpha="255" label="TABAC" value="63"/>
          <item color="#7aebe0" alpha="255" label="LUPIN_P" value="64"/>
          <item color="#7bc3b2" alpha="255" label="LUPIN_H" value="65"/>
          <item color="#c500ff" alpha="255" label="AUTR_PROT" value="66"/>
          <item color="#ff199b" alpha="255" label="AUTR_OLEA" value="67"/>
          <item color="#d3272a" alpha="255" label="SERRES" value="68"/>
          <item color="#e5dcc5" alpha="255" label="SEIGLE_P" value="69"/>
          <item color="#170f43" alpha="255" label="HOUBLON" value="70"/>
          <item color="#14f600" alpha="255" label="FEVE" value="71"/>
          <item color="#754600" alpha="255" label="CAFE_CACA" value="72"/>
          <item color="#7c7977" alpha="255" label="SOL_NU" value="73"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeRed="255" colorizeGreen="128" colorizeBlue="128" colorizeOn="0" grayscaleMode="0" colorizeStrength="100" saturation="0"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
